#pragma once

#include <algorithm>
#include <cstdint>
#include <iterator>
#include <memory>
#include <random>
#include <string>
#include <vector>

#include <boost/math/constants/constants.hpp>
#include <boost/multiprecision/cpp_bin_float.hpp>
#include <boost/multiprecision/mpfr.hpp>

using std::shared_ptr;
using std::vector;

using std::for_each;

using namespace boost::multiprecision;

typedef cpp_bin_float_double MP_TYPE;

const MP_TYPE two_pi = boost::math::constants::two_pi<MP_TYPE>();

template <typename dataType> class Generator {
public:
    virtual dataType getSample() = 0;
    virtual void getSamples(size_t nSamples, dataType* buf);

    virtual float getFrequency() const;
    virtual void setFrequency(float frequency);

    virtual float getAmplitude() const;
    virtual void setAmplitude(float amplitude);

    virtual float getAmplitudeDb() const;
    virtual void setAmplitudeDb(float amplitude);

    virtual bool isEnabled() const;
    virtual void enable();
    virtual void disable();

    virtual ~Generator();

    // disallow copy of virtual interface
    Generator(Generator const&) = delete;
    Generator& operator=(Generator const&) = delete;

protected:
    Generator(uint32_t _sampleRate, float _frequency, float _amplitude = 1);
    uint32_t sampleRate;
    float frequency;
    float amplitude;
    bool enabled;

private:
};

template <typename dataType> class SineGenerator : public Generator<dataType> {
public:
    SineGenerator(uint32_t _sampleRate, float _frequency, float _amplitude = 1,
        bool caching = true);

    dataType getSample() final;

    void setFrequency(float _frequency) final;
    void setFrequency(MP_TYPE frequency);

    void setAmplitude(float amplitude);

    virtual ~SineGenerator();

private:
    MP_TYPE instPhase;
    MP_TYPE phaseIncr;

    void dirtyCache();
    dataType unCachedGetSample();
    dataType cachedGetSample();

    vector<dataType> sampleCache;
    typename vector<dataType>::const_iterator cachePos;
    const bool caching;
    bool cacheInvalid;
};

template <typename dataType> class NoiseGenerator : public Generator<dataType> {
public:
    enum NOISE_TYPE {
        WHITE,
    };

    NoiseGenerator(uint32_t _sampleRate, NOISE_TYPE type, float _amplitude = 1);

    dataType getSample() final;
    virtual ~NoiseGenerator();

private:
    std::mt19937_64 rand_gen;
};

template <typename dataType> class JTestGenerator : public Generator<dataType> {
public:
    JTestGenerator(uint32_t sampleRate, uint8_t nbits);

    dataType getSample() final;
    virtual ~JTestGenerator();

private:
    std::vector<dataType> loop;
    typename std::vector<dataType>::const_iterator pos;
};

template <typename dataType> class SweepGenerator : public Generator<dataType> {
public:
    SweepGenerator(uint32_t sampleRate, float startFreq, float endFreq,
        float length, float amplitude = 1);
    virtual ~SweepGenerator();

    dataType getSample() final;
    float getFrequency() const;

    void setAmplitude(float amplitude);
    void setFrequency(float frequency);

private:
    SineGenerator<dataType> sg;
    float length;
    MP_TYPE startFreq;
    MP_TYPE curFreq;
    MP_TYPE freqStep;
};

template <typename dataType> class SineSynth {
public:
    SineSynth(uint32_t _sampleRate);

    void addSynth(shared_ptr<Generator<dataType>>);
    vector<shared_ptr<Generator<dataType>>> getSynths() const;
    void clearSynths();

    void getSamples(size_t nSamples, dataType* buf);

private:
    uint32_t sampleRate;
    vector<shared_ptr<Generator<dataType>>> generators;
};