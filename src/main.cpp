#include <cassert>
#include <chrono>
#include <iostream>
#include <memory>
#include <string>
#include <thread>

#include <curses.h>
#include <pulse/error.h>
#include <pulse/simple.h>

#include "SineSynth.h"
#include "WavWriter.h"

WINDOW* win;

constexpr int SAMPLERATE = 192000;
constexpr size_t BUFSIZE = 512;

typedef uint32_t SAMPTYPE;

WINDOW* init_curses()
{
    WINDOW* win = initscr();
    nodelay(win, TRUE);
    keypad(win, TRUE);
    cbreak();

    return win;
}

pa_simple* init_pulse()
{
    static const pa_sample_spec ss
        = { .format = PA_SAMPLE_S32NE, .rate = SAMPLERATE, .channels = 1 };

    int error = 0;
    pa_simple* s = pa_simple_new(NULL, "sinesynth", PA_STREAM_PLAYBACK, NULL,
        "playback", &ss, NULL, NULL, &error);
    if (error) {
        std::cerr << "Error initializing PulseAudio: " << pa_strerror(error)
                  << " (" << error << ")" << std::endl;
        exit(-1);
    }

    return s;
}

template <typename T> void smpte_imd_test(SineSynth<T>& synth)
{
    synth.clearSynths();
    synth.addSynth(std::make_shared<SineGenerator<T>>(SAMPLERATE, 60, 0.8));
    synth.addSynth(std::make_shared<SineGenerator<T>>(SAMPLERATE, 7000, 0.2));
}

template <typename T> void ccif_imd_test(SineSynth<T>& synth, float amplitude)
{
    assert(amplitude >= 0 && amplitude <= 1);
    synth.clearSynths();
    synth.addSynth(
        std::make_shared<SineGenerator<T>>(SAMPLERATE, 19000, amplitude / 2));
    synth.addSynth(
       std::make_shared<SineGenerator<T>>(SAMPLERATE, 20000, amplitude / 2));
}

int main(int argc, char* argv[])
{
    SineSynth<SAMPTYPE> synth(SAMPLERATE);

    std::shared_ptr<Generator<SAMPTYPE>> gen
        = std::make_shared<SineGenerator<SAMPTYPE>>(SAMPLERATE, 1000, 1);
    // auto gen = std::make_shared<SweepGenerator<SAMPTYPE>>(SAMPLERATE, 10,
    // 10000, 5, 1);

    // auto gen2 = std::make_shared<JTestGenerator<uint32_t>>(SAMPLERATE, 32);
    // synth.addSynth(gen2);

    // smpte_imd_test(synth);

    synth.addSynth(gen);

    // ccif_imd_test(synth,1);

    // SAMPTYPE buf2[BUFSIZE];
    
    // WavWriter w("test.wav", WavWriter::WAVE_FORMAT_IEEE_FLOAT, 1, sizeof(SAMPTYPE) * 8, SAMPLERATE);
    // for (auto i = 0; i < SAMPLERATE / BUFSIZE; i++) {
    //     synth.getSamples(BUFSIZE, buf2);
    //     w.writeSamples(buf2, sizeof(buf2));
    // }

    // return 0;

    auto s = init_pulse();
    auto win = init_curses();

    mvwprintw(win, 0, 0, "Initialized\n");

    int error = 0;
    pa_usec_t latency = pa_simple_get_latency(s, &error);
    wprintw(win, "Error: %s (%d) Latency: %d usec\n", pa_strerror(error), error,
        latency);

    SAMPTYPE buf[BUFSIZE];
    long long i;
    mvwprintw(win, 2, 0, "Loops: ");
    mvwprintw(win, 3, 0, "Freq: ");
    mvwprintw(win, 4, 0, "Ampl: ");
    for (i = 0;; i++) {
        auto c = getch();
        switch (c) {
        case KEY_UP:
            gen->setFrequency(gen->getFrequency() + 100);
            break;
        case KEY_DOWN:
            gen->setFrequency(gen->getFrequency() - 100);
            break;
        case '+':
            gen->setAmplitudeDb(gen->getAmplitudeDb() + 1);
            break;
        case '-':
            gen->setAmplitudeDb(gen->getAmplitudeDb() - 1);
            break;
        case 's':
            mvwprintw(win, 0, 0, "MODE: Sine                            ");
            gen = std::make_shared<SineGenerator<SAMPTYPE>>(
                SAMPLERATE, gen->getFrequency(), gen->getAmplitude());
            synth.clearSynths();
            synth.addSynth(gen);
            break;
        case 'w':
            gen = std::make_shared<SweepGenerator<SAMPTYPE>>(
                SAMPLERATE, 10, 10000, 5, gen->getAmplitude());
            mvwprintw(win, 0, 0, "MODE: Sweep %d -> %d over %ds", 10, gen->getFrequency(), 5);
            synth.clearSynths();
            synth.addSynth(gen);
            break;
        case 'n':
            mvwprintw(win, 0, 0, "MODE: Noise (white)                   ");
            gen = std::make_shared<NoiseGenerator<SAMPTYPE>>(SAMPLERATE,
                NoiseGenerator<SAMPTYPE>::WHITE, gen->getAmplitude());
            synth.clearSynths();
            synth.addSynth(gen);
            break;
        case 'i':
            mvwprintw(win, 0, 0, "MODE: SMPTE IMD (4:1 60hz:7000hz)");
            smpte_imd_test(synth);
            break;
        case 'I':
            mvwprintw(win, 0, 0, "MODE: CCIF IMD (1:1 19000hz:20000hz)");
            ccif_imd_test(synth, gen->getAmplitude());
            break;
        case 'j':
            mvwprintw(
                win, 0, 0, "MODE: J-test                                ");
            gen = std::make_shared<JTestGenerator<SAMPTYPE>>(SAMPLERATE, 32);
            synth.clearSynths();
            synth.addSynth(gen);
            break;
        case 'q':
        case 'Q':
            endwin();
            return 0;
        }

        synth.getSamples(BUFSIZE, buf);
        pa_simple_write(s, buf, sizeof(buf), &error);
        mvwprintw(win, 2, 8, "%6d", i);
        mvwprintw(win, 3, 8, "%6.0f", gen->getFrequency());
        mvwprintw(win, 4, 9, "%1.3f (%f)", gen->getAmplitude(),
            20 * log10(gen->getAmplitude() / 1));
    }

    endwin();
}