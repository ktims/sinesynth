#include "SineSynth.h"

#include <cassert>
#include <fstream>
#include <iostream>
#include <limits>
#include <random>

#include <type_traits>

template <typename dataType>
Generator<dataType>::Generator(
    uint32_t _sampleRate, float _frequency, float _amplitude)
    : sampleRate(_sampleRate)
    , frequency(_frequency)
    , amplitude(_amplitude)
    , enabled(true)
{
}

template <typename dataType> Generator<dataType>::~Generator() {}

template <typename dataType>
void Generator<dataType>::getSamples(size_t nSamples, dataType* buf)
{
    for (; nSamples > 0; nSamples--) {
        *buf++ = getSample();
    }
}

template <typename dataType> float Generator<dataType>::getFrequency() const
{
    return this->frequency;
}

template <typename dataType>
void Generator<dataType>::setFrequency(float frequency)
{
    this->frequency = frequency;
}

template <typename dataType> float Generator<dataType>::getAmplitude() const
{
    return this->amplitude;
}

template <typename dataType> float Generator<dataType>::getAmplitudeDb() const
{
    return 10 * log10(this->getAmplitude());
}

template <typename dataType>
void Generator<dataType>::setAmplitude(float amplitude)
{
    assert(amplitude <= 1);
    this->amplitude = amplitude;
}

template <typename dataType>
void Generator<dataType>::setAmplitudeDb(float amplitude)
{
    assert(amplitude <= 0);
    this->setAmplitude(pow(10, amplitude / 10));
}

template <typename dataType> bool Generator<dataType>::isEnabled() const
{
    return enabled;
}

template <typename dataType> void Generator<dataType>::enable()
{
    enabled = true;
}

template <typename dataType> void Generator<dataType>::disable()
{
    enabled = false;
}

template <typename dataType>
SineGenerator<dataType>::SineGenerator(
    uint32_t _sampleRate, float _frequency, float _amplitude, bool caching)
    : Generator<dataType>(_sampleRate, _frequency, _amplitude)
    , caching(caching)
    , cacheInvalid(true)
{
    setFrequency(_frequency);
    // std::cout << "Constructed SineGenerator with freq " << this->frequency
    //   << " phaseIncr " << phaseIncr << " rads/cycle" << std::endl;
}

template <typename dataType> SineGenerator<dataType>::~SineGenerator() {}

template <typename dataType> void SineGenerator<dataType>::dirtyCache()
{
    if (caching) {
        cacheInvalid = true;
        sampleCache.clear();
        instPhase = 0;
    }
}

template <typename dataType>
dataType SineGenerator<dataType>::unCachedGetSample()
{
    static MP_TYPE sample;
    dataType val;

    sample = sin(instPhase) * this->getAmplitude();

    if constexpr (std::is_floating_point<dataType>::value)
        val = sample.convert_to<dataType>();
    else if constexpr (std::is_signed<dataType>::value) {
        sample *= std::numeric_limits<dataType>::max();
        val = sample.convert_to<dataType>();
    } else {
        if (sample > 0) {
            sample *= std::numeric_limits<dataType>::max() / 2;
            val = sample.convert_to<dataType>();
        } else {
            sample *= std::numeric_limits<dataType>::max() / 2;
            val = sample.convert_to<dataType>() * -1;
        }
    }

    instPhase += phaseIncr;

    return val;
}

template <typename dataType> dataType SineGenerator<dataType>::cachedGetSample()
{
    static MP_TYPE sample;
    
    if (!cacheInvalid) {
        if (cachePos == sampleCache.end())
            cachePos = sampleCache.begin();
        return *cachePos++;
    }

    sample = sin(instPhase) * this->getAmplitude();

    dataType val;

    if constexpr (std::is_floating_point<dataType>::value)
        val = sample.convert_to<dataType>();
    else if constexpr (std::is_signed<dataType>::value) {
        sample *= std::numeric_limits<dataType>::max();
        val = sample.convert_to<dataType>();
    } else {
        if (sample > 0) {
            sample *= std::numeric_limits<dataType>::max() / 2;
            val = sample.convert_to<dataType>();
        } else {
            sample *= std::numeric_limits<dataType>::max() / 2;
            val = sample.convert_to<dataType>() * -1;
        }
    }

    // Advance the phase for the next loop
    instPhase += phaseIncr;

    // If we're about to store a zero crossing and rising slope, we are at a
    // cycle boundary and a loop
    bool doneCaching = false;
    if (!sampleCache.empty()) {
        if constexpr (std::is_floating_point<dataType>::value)
            doneCaching = val == 0 && 0 > sampleCache.back();
        else
            doneCaching = val == 0
                && 0 > static_cast<typename std::make_signed<dataType>::type>(
                           sampleCache.back());
    }

    if (doneCaching) {
        cacheInvalid = false;
        cachePos = sampleCache.begin();
        return *cachePos++;
    }

    // Only store the samples we send
    sampleCache.push_back(val);

    return val;
}

template <typename dataType> dataType SineGenerator<dataType>::getSample()
{
    return (caching) ? cachedGetSample() : unCachedGetSample();
}

template <typename dataType>
void SineGenerator<dataType>::setFrequency(MP_TYPE frequency)
{
    phaseIncr = (two_pi * frequency) / this->sampleRate;
    this->frequency = frequency.convert_to<float>();
    dirtyCache();
}

template <typename dataType>
void SineGenerator<dataType>::setFrequency(float frequency)
{
    phaseIncr = (two_pi * frequency) / this->sampleRate;
    this->frequency = frequency;
    dirtyCache();
}

template <typename dataType>
void SineGenerator<dataType>::setAmplitude(float amplitude)
{
    Generator<dataType>::setAmplitude(amplitude);
    dirtyCache();
}

template <typename dataType>
SineSynth<dataType>::SineSynth(uint32_t sampleRate)
    : sampleRate(sampleRate)
{
}

template <typename dataType>
void SineSynth<dataType>::addSynth(shared_ptr<Generator<dataType>> synth)
{
    generators.push_back(synth);
}

template <typename dataType>
vector<shared_ptr<Generator<dataType>>> SineSynth<dataType>::getSynths() const
{
    return generators;
}

template <typename dataType> void SineSynth<dataType>::clearSynths()
{
    generators.resize(0);
}

template <typename dataType>
void SineSynth<dataType>::getSamples(size_t nSamples, dataType* buf)
{
    for (; nSamples > 0; nSamples--) {
        *buf = 0;
        for_each(std::begin(generators), std::end(generators),
            [buf](std::shared_ptr<Generator<dataType>> g) {
                if (g->isEnabled())
                    *buf += g->getSample();
            });
        buf++;
    }
}

template <typename dataType>
NoiseGenerator<dataType>::NoiseGenerator(
    uint32_t _sampleRate, NOISE_TYPE type, float _amplitude)
    : Generator<dataType>(_sampleRate, 0, _amplitude)
    , rand_gen()
{
}

template <typename dataType> NoiseGenerator<dataType>::~NoiseGenerator() {}

template <typename dataType> dataType NoiseGenerator<dataType>::getSample()
{
    if constexpr (std::is_floating_point<dataType>::value) {
        auto sample
            = std::uniform_real_distribution<dataType>(-this->getAmplitude(),
                std::nextafter(this->getAmplitude(),
                    std::numeric_limits<dataType>::max()))(rand_gen);
        return sample;
    } else {
        auto sample = std::uniform_int_distribution<dataType>(
            std::numeric_limits<dataType>::min(),
            std::numeric_limits<dataType>::max())(rand_gen);
        return sample;
    }
}

template <typename dataType>
JTestGenerator<dataType>::JTestGenerator(uint32_t sampleRate, uint8_t nbits)
    : Generator<dataType>(sampleRate, 0, 1)
{
    size_t loopLength;
    if (sampleRate % 250 == 0)
        loopLength = sampleRate / 250;
    else if (sampleRate % 245 == 0)
        loopLength = sampleRate / 245;
    else {
        std::cerr << "Unable to generate non-integral fundamental frequency "
                     "for JTest";
        exit(-1);
    }

    this->loop.resize(loopLength);

    // Build signal
    const auto shift = nbits - 8;
    const uint8_t posbase = 0x40;
    const uint8_t negbase = 0xc0;

    const auto posval_first = posbase << shift;
    const auto negval_first = negbase << shift;
    const auto posval_last = posval_first - 1;
    const auto negval_last = negval_first - 1;

    for (auto i = 0; i < loopLength / 2; i += 4) {
        this->loop[i + 0] = negval_first;
        this->loop[i + 1] = negval_first;
        this->loop[i + 2] = posval_first;
        this->loop[i + 3] = posval_first;
    }
    for (auto i = loopLength / 2; i < loopLength; i += 4) {
        this->loop[i + 0] = negval_last;
        this->loop[i + 1] = negval_last;
        this->loop[i + 2] = posval_last;
        this->loop[i + 3] = posval_last;
    }

    this->pos = this->loop.begin();
}

template <typename dataType> dataType JTestGenerator<dataType>::getSample()
{
    if (this->pos == this->loop.end())
        this->pos = this->loop.begin();
    return *(this->pos++);
}

template <typename dataType> JTestGenerator<dataType>::~JTestGenerator() {}

template <typename dataType>
SweepGenerator<dataType>::SweepGenerator(uint32_t sampleRate, float startFreq,
    float endFreq, float length, float amplitude)
    : Generator<dataType>(sampleRate, endFreq, amplitude)
    , sg(sampleRate, startFreq, amplitude, false)
    , startFreq(startFreq)
    , curFreq(startFreq)
    , length(length)
{
    // Linear frequency step = ((endFreq - startFreq) / length) / sampleRate
    freqStep = ((this->getFrequency() - startFreq) / length) / sampleRate;
}

template <typename dataType> SweepGenerator<dataType>::~SweepGenerator() {}

template <typename dataType> dataType SweepGenerator<dataType>::getSample()
{
    dataType sample = sg.getSample();
    curFreq += freqStep;
    if (curFreq > this->getFrequency())
        curFreq = startFreq;
    sg.setFrequency(curFreq);
    return sample;
}

template <typename dataType>
float SweepGenerator<dataType>::getFrequency() const
{
    return Generator<dataType>::getFrequency();
}

template <typename dataType>
void SweepGenerator<dataType>::setAmplitude(float amplitude)
{
    sg.setAmplitude(amplitude);
    this->amplitude = amplitude;
}

template <typename dataType>
void SweepGenerator<dataType>::setFrequency(float frequency)
{
    Generator<dataType>::setFrequency(frequency);
    curFreq = startFreq;
    freqStep = ((this->getFrequency() - startFreq) / length) / this->sampleRate;
    sg.setFrequency(curFreq);
}

template class NoiseGenerator<float>;
template class SineGenerator<float>;
template class SweepGenerator<float>;
template class SineSynth<float>;

template class NoiseGenerator<uint32_t>;
template class SineSynth<uint32_t>;
template class SineGenerator<uint32_t>;
template class JTestGenerator<uint32_t>;
template class SweepGenerator<uint32_t>;